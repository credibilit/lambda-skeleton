# Sample AWS Lambda function to test build and deployment

This is a very simple and dummy function to allow test the pipelines for build
and deploy on Jenkins.

It uses the [AWS SAM](https://github.com/awslabs/serverless-application-model)
tool to test it locally, but not for packaging and deploy since we already have
the process and toolset to do that.

## Testing

```
echo '{ "foo": "bar" }' | sam local invoke skeleton
```

## Build

```
make build VERSION=<put the version here>
```
