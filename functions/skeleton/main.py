import os

def handler(event, context):
    print("Skeleton function is running...")
    print("Event:")
    for var, value in event.items():
        print("- {} = {}".format(var, value))
    print("\nEnvironment variables:")
    for var, value in os.environ.items():
        print("- {} = {}".format(var, value))

    return 'OK'

# To test locally, lambda will ignore it
if __name__ == "__main__":
    handler({'foo': 'bar'}, None)
